name             'gitlab-omnibus-builder'
maintainer       'GitLab B.V.'
maintainer_email 'jacob@gitlab.com'
license          'MIT'
description      'Installs/Configures gitlab-omnibus-builder'
long_description 'Installs/Configures gitlab-omnibus-builder'
version          '0.2.6'

depends 'gitlab-attributes-with-secrets'
