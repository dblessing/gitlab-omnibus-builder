# Recipe: default
#
# This recipe prepares an omnibus-gitlab build environment. If you also want to
# use GitLab CI for automated builds, use the 'ci' recipe instead.

# Install git because we need it in this cookbook, for the omnibus builds.
package 'git'

# We need Ruby to run omnibus
include_recipe 'gitlab-omnibus-builder::ruby'

# We need Go (golang) to compile gitlab-git-http-server
include_recipe 'gitlab-omnibus-builder::go'

# Set up dependencies, user and directories for the omnibus builds
include_recipe 'gitlab-omnibus-builder::omnibus-build'
