# Recipe: ci
#
# This recipe sets up a CI-controlled omnibus-gitlab build environment. If you
# do not want or need GitLab CI, use the 'default' recipe instead.

include_recipe 'gitlab-omnibus-builder::default'

# Set up gitlab-ci-multi-runner, so we can trigger builds from GitLab CI
include_recipe 'gitlab-omnibus-builder::gitlab-ci-multi-runner'

# Install secrets for fetching EE source code, pushing packages
include_recipe 'gitlab-omnibus-builder::secrets'
