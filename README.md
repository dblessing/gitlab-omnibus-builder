# gitlab-omnibus-builder-cookbook

This cookbook sets up build machines for omnibus-gitlab.

## Supported Platforms

Works on Ubuntu 12.04, 14.04, Debian 7, Centos 6, Centos 7.

## Recipe: default

Create a build environment without setting up CI. To set up a build environment
without a Chef server you can use the following script.

Note that on RHEL 7, you need to add EPEL for the 'fakeroot' dependency.

```
(
set -e
set -u

# Install the Chef client
curl -L https://www.chef.io/chef/install.sh | sudo bash

# Download the gitlab-omnibus-builder cookbook using Git and run its 'default' recipe
chef_root=/tmp/gitlab-omnibus-builder.$$
mkdir "$chef_root"
cd "$chef_root"
mkdir cookbooks
git clone https://gitlab.com/gitlab-org/gitlab-omnibus-builder.git cookbooks/gitlab-omnibus-builder
/opt/chef/bin/chef-client -z -r 'recipe[gitlab-omnibus-builder::default]'
)
```

## Recipe: ci

Set up a GitLab runner for integration with GitLab CI. Includes the 'default'
recipe.

## Manual setup steps (CI)

Once you have provisioned an omnibus build box, you need to do the following
manual setup steps.

First, register the builder as a GitLab CI Multi Runner with the coordinator of your
choice.

```
# on the new build box
sudo su - gitlab_ci_multi_runner
cd /home/gitlab_ci_multi_runner
# enter the CI url and token
gitlab-ci-multi-runner register
```

Second, drop in the secrets needed to clone GitLab EE and upload packages to
AWS.

```
# In the chef-repo:
bundle exec rake edit_role_secrets[omnibus-builder]
# No need to edit the secrets, just save and exit. This makes sure the new node
# can access the secrets.
```

## Attributes

## Usage

### gitlab-omnibus-builder::default

Include `gitlab-omnibus-builder` in your node's `run_list`:

```json
{
  "run_list": [
    "recipe[gitlab-omnibus-builder::default]"
  ]
}
```

## License and Authors

This cookbook is distributed under the MIT license, see LICENSE.
